﻿import { Component, ElementRef, Renderer2, ViewChild, Input } from '@angular/core';
import { FirebaseApp } from 'angularfire2';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase';
declare const navigator: any;
declare const MediaRecorder: any;
declare var MediaStreamRecorder: any;

@Component({
    selector: 'record',
    templateUrl: 'record.component.html'
})
export class RecordComponent {
    @Input() chunkDuration: number = 5 * 1000;
    @Input() mimeType: string = 'audio/wav';
    @ViewChild('recordButton') record: ElementRef;
    @ViewChild('stopRecordButton') stop: ElementRef;
    @ViewChild('clocker') clocker: ElementRef;
    private uploadTask: firebase.storage.UploadTask;

    constructor(
        public db: AngularFireDatabase
    ) {
    }

    ngAfterViewInit() {

        let mediaRecorder: any;
        let chunks: any = [];
        let startTime: any;
        let endTime: any;

        let totalSeconds: number = 0;

        let timerSub: any;

        const mediaConstraints = {
            audio: true
        };

        navigator.getUserMedia = ((<any>navigator).getUserMedia ||
            (<any>navigator).webkitGetUserMedia ||
            (<any>navigator).mozGetUserMedia ||
            (<any>navigator).msGetUserMedia);

        if (navigator.getUserMedia) {
            console.log('getUserMedia supported.');

            var onSuccess = (stream: any) => {
                mediaRecorder = new MediaStreamRecorder(stream);
                mediaRecorder.stream = stream;
                mediaRecorder.mimeType = this.mimeType;

                this.record.nativeElement.onclick = () => {
                    startTime = +Date.now();

                    mediaRecorder.start(this.chunkDuration); //in millisecond

                    totalSeconds = 0;
                    timerSub = setInterval(() => {
                        totalSeconds++;
                        console.log(totalSeconds);
                        var hour = Math.floor(totalSeconds / 3600);
                        var minute = Math.floor((totalSeconds - hour * 3600) / 60);
                        var seconds = totalSeconds - (hour * 3600 + minute * 60);
                        this.clocker.nativeElement.innerHTML = hour + ":" + minute + ":" + seconds;
                    }, 1000);

                    this.record.nativeElement.style.background = "red";
                    this.record.nativeElement.disabled = true;
                    this.stop.nativeElement.disabled = false;
                }

                this.stop.nativeElement.onclick = () => {

                    mediaRecorder.stop();
                    mediaRecorder.stream.stop();

                    clearInterval(timerSub);

                    this.record.nativeElement.style.background = "";
                    this.record.nativeElement.disabled = false;
                    this.stop.nativeElement.disabled = true;
                }

                mediaRecorder.onstop = () => {
                    endTime = +Date.now();

                    console.log(stream);
                    var clipName = Math.random().toString(36).substr(2, 9) + '.' + this.mimeType.split('/').pop();
                    console.log(clipName);
                    let blob = new Blob(chunks, { type: this.mimeType + '; codecs:opus' });
                    chunks = [];
                    console.log("recorder stopped");

                    let storageRef = firebase.storage().ref();
                    this.uploadTask = storageRef.child('files/' + clipName).put(blob);

                    this.uploadTask.then((response: any) => {
                        firebase.database().ref('log').push({
                            FileName: clipName,
                            Time: startTime,
                            Duration: (endTime - startTime) / 1000,
                            Path: this.uploadTask.snapshot.downloadURL
                        });
                    });
                };

                mediaRecorder.ondataavailable = (blob: any) => {
                    chunks.push(blob);
                };
            }
            navigator.getUserMedia(mediaConstraints, onSuccess, this.onMediaError);

        }
    }

    updateTimer(clocker: any, totalSeconds: number) {
        if (clocker) {
            console.log(clocker);

        }
    }

    onMediaError(err: any) {
        console.log('The following error occured: ' + err);
    }

}