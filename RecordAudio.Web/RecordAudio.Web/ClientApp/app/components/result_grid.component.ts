﻿import { Component, OnInit, OnDestroy } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { RecordModel } from './../model/record.model';
import { AppServices } from './../service/app.service';

@Component({
    selector: 'result-grid',
    templateUrl: './result_grid.component.html',
    styleUrls: ['./result_grid.component.css']
})

export class ResultGridComponent {
    public data: Observable<RecordModel[]>;
    public servicesSubcription: any;

    constructor(private services: AppServices) {
    }

    ngOnInit() {
        this.data = this.services.getRecordMetaData();
    }

    ngOnDestroy() {
        //this.servicesSubcription.unsubcribe();
    }

}
