﻿import { Http, RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/map'

import { AngularFireDatabase } from 'angularfire2/database';

import { RecordModel, MOCKRECORD } from '../model/record.model'

@Injectable()
export class AppServices {

    constructor(private http: Http,
        private db: AngularFireDatabase
    ) {
    }

    public getRecordMetaData(): any {
        //let options = new RequestOptions({
        //    headers: new Headers({
        //        'Accept': 'application/json',
        //        'Access-Control-Allow-Origin': '*',
        //        'Access-Control-Allow-Methods': 'GET,HEAD,OPTIONS,POST,PUT',
        //        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
        //    })
        //});
      
        //return this.http.get(this.jsonAPI)
        //    .map((res: any) => {
        //        const data = res.json();
        //        return data;
        //    });
        return this.db.list('log').valueChanges();
    }
}