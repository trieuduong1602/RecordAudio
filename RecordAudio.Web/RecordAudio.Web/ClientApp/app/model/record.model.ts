﻿export class RecordModel {
    FileName: string;
    Time: string;
    Duration: number;
    Path: string;
}

export const MOCKRECORD: RecordModel[] = [
    {
        FileName: "1_us_fast_join",
        Time: "12/7/2017 11:20:30 PM",
        Duration: 2,
        Path: "D:\\Project\\Research\\RecordAudioWeb\\RecordAudio.Web\\RecordAudio.Web\\RecordFiles\\files\\1_us_fast_join.mp3"
    },
    {
        FileName: "1_us_fast_join",
        Time: "12/7/2017 11:20:30 PM",
        Duration: 2,
        Path: "D:\\Project\\Research\\RecordAudioWeb\\RecordAudio.Web\\RecordAudio.Web\\RecordFiles\\files\\1_us_fast_join.mp3"
    }
];