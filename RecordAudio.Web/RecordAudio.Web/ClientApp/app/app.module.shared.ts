import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AngularFireModule } from 'angularfire2';

// New imports to update based on AngularFire2 version 4
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { AppComponent } from './components/app/app.component';
import { MainComponent } from './components/main/main.component';
import { RecordComponent } from './components/main/record.component';
import { firebaseConfig } from './firebase.config';
import { ResultGridComponent } from './components/result_grid.component';

import { AppServices } from './service/app.service';

@NgModule({
    declarations: [
        AppComponent,
        MainComponent,
        RecordComponent,
        ResultGridComponent
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'main', component: MainComponent },
            { path: '**', redirectTo: 'home' }
        ]),
        AngularFireModule.initializeApp(firebaseConfig),
        AngularFireDatabaseModule,
        AngularFireAuthModule
    ],
    providers: [AppServices]
})
export class AppModuleShared {
}
